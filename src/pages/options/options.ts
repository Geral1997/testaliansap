import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';


@IonicPage()
@Component({
  selector: 'page-options',
  templateUrl: 'options.html',
})
export class OptionsPage {

  selectedItem: any;
  icons: string[];
  title: string[];
  items: Array<{title: string, icon: string}>;

  constructor(
    public navCtrl: NavController,
     public navParams: NavParams) {

    //declaracion de arreglos con el contenido de la lista
    this.icons = ['home', 'basket', 'clock', 'cube', 
      'pricetags', 'warning','nuclear'];

    this.title =["Principal","Pedido Sugerido","Historial de pedidos",
      "Entregas del día","Cupones", "Reclamos", "Faltantes"];
    
    this.items = [];

      //ciclo que adiciona el nombre y el icono a cada item
    for (let i = 0; i < 7; i++) {
      this.items.push({
        title: this.title[i],
        icon: this.icons[i]
      });
    }
  }

  itemTapped(event, item) {
    //agregar los items en la vista (options.html)
    this.navCtrl.push(OptionsPage, {
      item: item
    });
  }

}
