import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';


import { AlertController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { OptionsPage } from '../options/options';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  users:any;

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public api:ApiProvider) {
      this.getUsers();
    }


    //metodo que trae los datos a traves del provider "api"
    getUsers(){
      this.api.getUsers()
    .then(data => {
      this.users = data;
      console.log(this.users);
    });
    }

  //metodo que muestra un Confirm alert
  doConfirm(){
    let confirm = this.alertCtrl.create({
      title: 'Pedido',
      message: 'Desea realizar el pedido?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Si',
          handler: () => {
            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present()
  }

  goTo(){
    this.navCtrl.push(OptionsPage);
  }

}
