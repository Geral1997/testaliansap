import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Validators , FormBuilder, FormGroup } from '@angular/forms';
import { HomePage } from '../home/home';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  
  loginForm: FormGroup;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public alertCtrl: AlertController) {

      //creacion de validaciones para formulario
      this.loginForm = formBuilder.group({
        'username': [
          '',
          Validators.compose([Validators.required, Validators.minLength(3)])
        ],
        'password':[
          '',
          Validators.compose([Validators.required, Validators.minLength(3)])
        ]
      });
  }

  //metodo boolean que valida el formulario
  validate(): boolean {
    if(this.loginForm.valid){
      return true;
    }

    //mensaje de error
    let errorMsg = 'Los campos deben tener mínimo 3 caracteres';

    //validación campo usuario
    let controlU = this.loginForm.controls['username'];
    if(!controlU.valid){
      if(controlU.errors['required']){
        errorMsg= 'Ingrese el usuario';
      } else if (controlU.errors['minlength']){
        errorMsg = 'El usuario debe tener mínimo 3 caracteres'
      }
    }

    //validación campo contraseña
    let controlP = this.loginForm.controls['password'];
    if(!controlP.valid){
      if(controlP.errors['required']){
        errorMsg= 'Ingrese la contraseña';
      } else if (controlP.errors['minlength']){
        errorMsg = 'La contraseña debe tener mínimo 3 caracteres'
      }
    }

  }

  //validadas las credenciales ir a pag de categorias
  submit():void {
    if (this.validate()){
      this.navCtrl.push(HomePage);
    }else{
      let alert = this.alertCtrl.create({
        title: 'error',
        subTitle: 'Verifique que ambos campos tengan al menos 3 caracteres',
        buttons: ['OK']
      });
      alert.present();
    }
  }

}
