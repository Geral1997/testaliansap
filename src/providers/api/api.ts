import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable()
export class ApiProvider {

  //Url del API
  API:string = "https://jsonplaceholder.typicode.com/posts";

  constructor(public http: HttpClient) {
    console.log('Hello ApiProvider');
  }

  //metodo que accede al API y trae los datos por GET
  getUsers(){
    
    return new Promise(resolve => {
      this.http.get(this.API).subscribe( data =>{
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

}
